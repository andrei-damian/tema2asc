"""
bariera
"""

from threading import Semaphore, Lock


class ReusableBarrierSem(object):
    """ Bariera reentranta, implementata folosind semafoare """

    def __init__(self, num_threads):
        self.num_threads = num_threads
        self.count_threads1 = self.num_threads
        self.count_threads2 = self.num_threads
        self.counter_lock = Lock()  # protejam accesarea/modificarea contoarelor
        self.threads_sem1 = Semaphore(0)  # blocam thread-urile in prima etapa
        self.threads_sem2 = Semaphore(0)  # blocam thread-urile in a doua etapa

    def wait(self):
        """
        wait
        """
        self.phase1()
        self.phase2()

    def phase1(self):
        """
        phase1
        """
        with self.counter_lock:
            self.count_threads1 -= 1
            if self.count_threads1 == 0:
                for i in range(self.num_threads):
                    self.threads_sem1.release()
                self.count_threads1 = self.num_threads

        self.threads_sem1.acquire()

    def phase2(self):
        """
        phase2
        """
        with self.counter_lock:
            self.count_threads2 -= 1
            if self.count_threads2 == 0:
                for i in range(self.num_threads):
                    self.threads_sem2.release()
                self.count_threads2 = self.num_threads

        self.threads_sem2.acquire()
